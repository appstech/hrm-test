<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170814064911 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE blt_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_3DDD40A692FC23A8 (username_canonical), UNIQUE INDEX UNIQ_3DDD40A6A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_3DDD40A6C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blt_branch (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, orgnization_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, fax VARCHAR(255) DEFAULT NULL, mobile VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, zip_code VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, latitude VARCHAR(255) DEFAULT NULL, longitude VARCHAR(255) DEFAULT NULL, status INT DEFAULT 1 NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_7B0C72035E237E06 (name), INDEX IDX_7B0C720361220EA6 (creator_id), INDEX IDX_7B0C7203B54BA39E (orgnization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blt_department (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, orgnization_id INT DEFAULT NULL, branch_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, status INT DEFAULT 1 NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_CADC4B735E237E06 (name), UNIQUE INDEX UNIQ_CADC4B73989D9B62 (slug), UNIQUE INDEX UNIQ_CADC4B73E7927C74 (email), INDEX IDX_CADC4B7361220EA6 (creator_id), INDEX IDX_CADC4B73B54BA39E (orgnization_id), INDEX IDX_CADC4B73DCD6CC49 (branch_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blt_designation (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, orgnization_id INT DEFAULT NULL, branch_id INT DEFAULT NULL, department_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, status INT DEFAULT 1 NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_4D21EA1F5E237E06 (name), INDEX IDX_4D21EA1F61220EA6 (creator_id), INDEX IDX_4D21EA1FB54BA39E (orgnization_id), INDEX IDX_4D21EA1FDCD6CC49 (branch_id), INDEX IDX_4D21EA1FAE80F5DF (department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blt_job (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, orgnization_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, min_salary INT DEFAULT 0, max_salary INT DEFAULT 0, status INT DEFAULT 1 NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_65D62D822B36786B (title), INDEX IDX_65D62D8261220EA6 (creator_id), INDEX IDX_65D62D82B54BA39E (orgnization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blt_organization (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, unique_id VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, tag_line LONGTEXT DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, started_at DATETIME DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, registeration_number VARCHAR(65) DEFAULT NULL, national_tax_number VARCHAR(65) DEFAULT NULL, vision LONGTEXT DEFAULT NULL, mission LONGTEXT DEFAULT NULL, status INT DEFAULT 1 NOT NULL, created_at DATETIME NOT NULL, modified_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_329374BF5E237E06 (name), UNIQUE INDEX UNIQ_329374BFE3C68343 (unique_id), UNIQUE INDEX UNIQ_329374BFE7927C74 (email), INDEX IDX_329374BF61220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blt_branch ADD CONSTRAINT FK_7B0C720361220EA6 FOREIGN KEY (creator_id) REFERENCES blt_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_branch ADD CONSTRAINT FK_7B0C7203B54BA39E FOREIGN KEY (orgnization_id) REFERENCES blt_organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_department ADD CONSTRAINT FK_CADC4B7361220EA6 FOREIGN KEY (creator_id) REFERENCES blt_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_department ADD CONSTRAINT FK_CADC4B73B54BA39E FOREIGN KEY (orgnization_id) REFERENCES blt_organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_department ADD CONSTRAINT FK_CADC4B73DCD6CC49 FOREIGN KEY (branch_id) REFERENCES blt_branch (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_designation ADD CONSTRAINT FK_4D21EA1F61220EA6 FOREIGN KEY (creator_id) REFERENCES blt_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_designation ADD CONSTRAINT FK_4D21EA1FB54BA39E FOREIGN KEY (orgnization_id) REFERENCES blt_organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_designation ADD CONSTRAINT FK_4D21EA1FDCD6CC49 FOREIGN KEY (branch_id) REFERENCES blt_branch (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_designation ADD CONSTRAINT FK_4D21EA1FAE80F5DF FOREIGN KEY (department_id) REFERENCES blt_department (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_job ADD CONSTRAINT FK_65D62D8261220EA6 FOREIGN KEY (creator_id) REFERENCES blt_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_job ADD CONSTRAINT FK_65D62D82B54BA39E FOREIGN KEY (orgnization_id) REFERENCES blt_organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_organization ADD CONSTRAINT FK_329374BF61220EA6 FOREIGN KEY (creator_id) REFERENCES blt_user (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blt_branch DROP FOREIGN KEY FK_7B0C720361220EA6');
        $this->addSql('ALTER TABLE blt_department DROP FOREIGN KEY FK_CADC4B7361220EA6');
        $this->addSql('ALTER TABLE blt_designation DROP FOREIGN KEY FK_4D21EA1F61220EA6');
        $this->addSql('ALTER TABLE blt_job DROP FOREIGN KEY FK_65D62D8261220EA6');
        $this->addSql('ALTER TABLE blt_organization DROP FOREIGN KEY FK_329374BF61220EA6');
        $this->addSql('ALTER TABLE blt_department DROP FOREIGN KEY FK_CADC4B73DCD6CC49');
        $this->addSql('ALTER TABLE blt_designation DROP FOREIGN KEY FK_4D21EA1FDCD6CC49');
        $this->addSql('ALTER TABLE blt_designation DROP FOREIGN KEY FK_4D21EA1FAE80F5DF');
        $this->addSql('ALTER TABLE blt_branch DROP FOREIGN KEY FK_7B0C7203B54BA39E');
        $this->addSql('ALTER TABLE blt_department DROP FOREIGN KEY FK_CADC4B73B54BA39E');
        $this->addSql('ALTER TABLE blt_designation DROP FOREIGN KEY FK_4D21EA1FB54BA39E');
        $this->addSql('ALTER TABLE blt_job DROP FOREIGN KEY FK_65D62D82B54BA39E');
        $this->addSql('DROP TABLE blt_user');
        $this->addSql('DROP TABLE blt_branch');
        $this->addSql('DROP TABLE blt_department');
        $this->addSql('DROP TABLE blt_designation');
        $this->addSql('DROP TABLE blt_job');
        $this->addSql('DROP TABLE blt_organization');
    }
}
