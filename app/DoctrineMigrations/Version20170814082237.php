<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170814082237 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blt_job ADD branch_id INT DEFAULT NULL, ADD department_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE blt_job ADD CONSTRAINT FK_65D62D82DCD6CC49 FOREIGN KEY (branch_id) REFERENCES blt_branch (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_job ADD CONSTRAINT FK_65D62D82AE80F5DF FOREIGN KEY (department_id) REFERENCES blt_department (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_65D62D82DCD6CC49 ON blt_job (branch_id)');
        $this->addSql('CREATE INDEX IDX_65D62D82AE80F5DF ON blt_job (department_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blt_job DROP FOREIGN KEY FK_65D62D82DCD6CC49');
        $this->addSql('ALTER TABLE blt_job DROP FOREIGN KEY FK_65D62D82AE80F5DF');
        $this->addSql('DROP INDEX IDX_65D62D82DCD6CC49 ON blt_job');
        $this->addSql('DROP INDEX IDX_65D62D82AE80F5DF ON blt_job');
        $this->addSql('ALTER TABLE blt_job DROP branch_id, DROP department_id');
    }
}
