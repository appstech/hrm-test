<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170814103304 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE blt_employee_history (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, creator_id INT DEFAULT NULL, orgnization_id INT DEFAULT NULL, branch_id INT DEFAULT NULL, department_id INT DEFAULT NULL, designation_id INT DEFAULT NULL, salary INT DEFAULT 0, start_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, INDEX IDX_644276448C03F15C (employee_id), INDEX IDX_6442764461220EA6 (creator_id), INDEX IDX_64427644B54BA39E (orgnization_id), INDEX IDX_64427644DCD6CC49 (branch_id), INDEX IDX_64427644AE80F5DF (department_id), INDEX IDX_64427644FAC7D83F (designation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blt_employee_history ADD CONSTRAINT FK_644276448C03F15C FOREIGN KEY (employee_id) REFERENCES blt_employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_employee_history ADD CONSTRAINT FK_6442764461220EA6 FOREIGN KEY (creator_id) REFERENCES blt_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_employee_history ADD CONSTRAINT FK_64427644B54BA39E FOREIGN KEY (orgnization_id) REFERENCES blt_organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_employee_history ADD CONSTRAINT FK_64427644DCD6CC49 FOREIGN KEY (branch_id) REFERENCES blt_branch (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_employee_history ADD CONSTRAINT FK_64427644AE80F5DF FOREIGN KEY (department_id) REFERENCES blt_department (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blt_employee_history ADD CONSTRAINT FK_64427644FAC7D83F FOREIGN KEY (designation_id) REFERENCES blt_designation (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE blt_employee_history');
    }
}
