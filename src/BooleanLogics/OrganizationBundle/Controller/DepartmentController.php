<?php

namespace BooleanLogics\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use BooleanLogics\OrganizationBundle\Entity\Organization;
use BooleanLogics\OrganizationBundle\Entity\Branch;
use BooleanLogics\OrganizationBundle\Entity\Department;
use BooleanLogics\OrganizationBundle\Form\Type\DepartmentType;
use BooleanLogics\CoreBundle\Service\StringHelper;
use BooleanLogics\CoreBundle\Service\ResponseHelper;
use BooleanLogics\CoreBundle\Service\FormErrorsSerializer;
use BooleanLogics\CoreBundle\Service\SystemMessage;

class DepartmentController extends Controller {

    private $systemMessage;
    private $response;

    public function __construct() {
        $this->systemMessage = new SystemMessage();
        $this->response = new ResponseHelper();
    }

    /**
     * @Route("/department/create", name="department_new")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function newAction(request $request) {

        $department = new Department();

        $organization = $this->container->get('organization_manager')->getOrganization();

        $form = $this->createForm(DepartmentType::class, $department, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('department_new'),
            'organization' => $organization
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $department->setCreator($this->getUser());
                $department->setOrgnization($organization);
                $name = $department->getName();
                $slug = new StringHelper($name);
                $slug = $slug->getOrganizationID();
                $department->setSlug($slug);
                $em = $this->getDoctrine()->getManager();
                $em->persist($department);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Department', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return array('form' => $form->createView());
    }

}
