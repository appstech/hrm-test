<?php

namespace BooleanLogics\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use BooleanLogics\OrganizationBundle\Entity\Organization;
use BooleanLogics\OrganizationBundle\Entity\Branch;
use BooleanLogics\OrganizationBundle\Form\Type\BranchType;
use BooleanLogics\CoreBundle\Service\StringHelper;
use BooleanLogics\CoreBundle\Service\ResponseHelper;
use BooleanLogics\CoreBundle\Service\FormErrorsSerializer;
use BooleanLogics\CoreBundle\Service\SystemMessage;
use BooleanLogics\CoreBundle\Model\DatatableListManager;

class BranchController extends Controller {

    private $systemMessage;
    private $response;

    public function __construct() {
        $this->systemMessage = new SystemMessage();
        $this->response = new ResponseHelper();
    }

    /**
     * @Route("/branch/list", name="branch_list")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function listAction(Request $request) {
        $organization = $this->container->get('organization_manager')->getOrganization();
        return $this->render('BooleanLogicsOrganizationBundle:Branch:list.html.twig', array(
                    'listUrl' => $this->generateUrl('branch_get_list', array('organization' => $organization->getId()))
        ));
    }

    /**
     * @Route("/branch/get-list", name="branch_get_list")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function listAjaxAction(request $request) {


        if ($request->isXmlHttpRequest()) {
            $organization = $this->container->get('organization_manager')->getOrganization();
            $currentUser = $this->getUser();

            $datatableListManager = new DatatableListManager();
            $datatableParameters = $datatableListManager->sanitizeMemberListDatatable(
                    $request->request->get('draw'), $request->request->get('columns'), $request->request->get('order'), $request->request->get('start'), $request->request->get('length'), $request->request->get('search', null)
            );
            $repository = $this->getDoctrine()
                    ->getRepository('BooleanLogicsOrganizationBundle:Branch');
            $results = $repository->findPaginatedWithContact($datatableParameters['search'], $datatableParameters['order'], $datatableParameters['length'], $datatableParameters['start'], $organization);
            $returnData = array();
            $tagsData = array();

            foreach ($results['data'] as $result) {
                $tagsData = array();

                array_push($returnData, array(
                    'id' => $result->getId(),
                    'name' => $result->getName(),
                    'email' => $result->getEmail(),
                    'phone' => $result->getPhone(),
                    'mobile' => $result->getMobile(),
                    'fax' => $result->getFax(),
                    'city' => $result->getCity(),
                    'state' => $result->getState(),
                    'address' => $result->getAddress(),
                    'zipcode' => $result->getZipcode(),
                    'status' => $result->getStatusTxt(),
                ));
            }
            return new JsonResponse(array(
                'draw' => $datatableParameters['draw'],
                'recordsTotal' => $results['recordsTotal'],
                'recordsFiltered' => $results['recordsFiltered'],
                'data' => $returnData
            ));
        }
        throw $this->createNotFoundException();
    }

    /**
     * @Route("/branch/create", name="branch_new")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function newAction(request $request) {
        $branch = new Branch();

        $organization = $this->container->get('organization_manager')->getOrganization();

        $form = $this->createForm(BranchType::class, $branch, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('branch_new')
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {




            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $branch->setCreator($this->getUser());
                $branch->setOrgnization($organization);
                $name = $branch->getName();
                $slug = new StringHelper($name);
                $slug = $slug->getOrganizationID();
                $branch->setSlug($slug);
                $em = $this->getDoctrine()->getManager();
                $em->persist($branch);

                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Branch', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return array('form' => $form->createView());
    }

}
