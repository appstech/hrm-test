<?php

namespace BooleanLogics\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use BooleanLogics\OrganizationBundle\Entity\Organization;
use BooleanLogics\OrganizationBundle\Entity\Branch;
use BooleanLogics\OrganizationBundle\Entity\Department;
use BooleanLogics\OrganizationBundle\Entity\Designation;
use BooleanLogics\OrganizationBundle\Form\Type\DesignationType;
use BooleanLogics\CoreBundle\Service\StringHelper;
use BooleanLogics\CoreBundle\Service\ResponseHelper;
use BooleanLogics\CoreBundle\Service\FormErrorsSerializer;
use BooleanLogics\CoreBundle\Service\SystemMessage;

class DesignationController extends Controller {

    private $systemMessage;
    private $response;

    public function __construct() {
        $this->systemMessage = new SystemMessage();
        $this->response = new ResponseHelper();
    }

    /**
     * @Route("/designation/create", name="designation_new")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function newAction(request $request) {

        $designation = new Designation();
        $organization = $this->container->get('organization_manager')->getOrganization();

        $form = $this->createForm(DesignationType::class, $designation, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('designation_new_save'),
            'organization' => $organization
        ));

        $form->handleRequest($request);
        $template = 'BooleanLogicsOrganizationBundle:Designation:new.html.twig';
        if ($request->isXmlHttpRequest()) { //AJAX request
            $template = 'BooleanLogicsOrganizationBundle:Designation:new-form-template.html.twig';
        }
        return $this->render($template, array(
                    'form' => $form->createView(),
                    'organization' => $organization
        ));
    }

    /**
     * @Route("/designation/save", name="designation_new_save")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function postSaveAction(request $request) {
        $designation = new Designation();
        $organization = $this->container->get('organization_manager')->getOrganization();
        $form = $this->createForm(DesignationType::class, $designation, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('designation_new_save'),
            'organization' => $organization
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $designation->setCreator($this->getUser());
                $designation->setOrgnization($organization);
                $em = $this->getDoctrine()->getManager();
                $em->persist($designation);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Designation', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        $template = 'BooleanLogicsOrganizationBundle:Designation:new.html.twig';

        return $this->render($template, array(
                    'form' => $form->createView(),
                    'organization' => $organization
        ));
    }

}
