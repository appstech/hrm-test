<?php

namespace BooleanLogics\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use BooleanLogics\OrganizationBundle\Entity\Organization;
use BooleanLogics\OrganizationBundle\Form\Type\OrganizationType;
use BooleanLogics\CoreBundle\Service\FileUploader;
use BooleanLogics\CoreBundle\Service\StringHelper;
use BooleanLogics\CoreBundle\Service\ResponseHelper;
use BooleanLogics\CoreBundle\Service\FormErrorsSerializer;
use BooleanLogics\CoreBundle\Service\SystemMessage;

class OrganizationController extends Controller {

    private $systemMessage;
    private $response;

    public function __construct() {
        $this->systemMessage = new SystemMessage();
        $this->response = new ResponseHelper();
    }

    /**
     * @Route("create", name="organization_new")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function newAction(request $request) {
        $organization = new Organization();

        $form = $this->createForm(OrganizationType::class, $organization, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('organization_new')
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $organizationName = $organization->getName();
                $organizationId = new StringHelper($organizationName);
                $organizationId = $organizationId->getOrganizationID();
                $organization->setUnique($organizationId);
                $organization->setCreator($this->getUser());

                if ($organization->getLogo()) { //Upload Logo File
                    $file = $organization->getLogo();
                    $fileUploader = new FileUploader($this->getParameter('assets_directory') . $organizationId);
                    $fileName = $fileUploader->upload($file);
                    $organization->setLogo($fileName);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($organization);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Organization', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return array('form' => $form->createView());
    }

}
