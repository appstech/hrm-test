<?php

namespace BooleanLogics\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

    public function indexAction() {
        return $this->render('BooleanLogicsOrganizationBundle:Default:index.html.twig');
    }

}
