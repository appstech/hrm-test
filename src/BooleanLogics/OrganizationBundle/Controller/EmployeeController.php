<?php

namespace BooleanLogics\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use BooleanLogics\OrganizationBundle\Entity\Organization;
use BooleanLogics\OrganizationBundle\Entity\Employee;
use BooleanLogics\OrganizationBundle\Entity\EmployeeHistory;
use BooleanLogics\OrganizationBundle\Entity\EmployeeEducation;
use BooleanLogics\OrganizationBundle\Entity\EmployeeCertificate;
use BooleanLogics\OrganizationBundle\Entity\EmployeeJobHistory;
use BooleanLogics\OrganizationBundle\Form\Type\EmployeeType;
use BooleanLogics\OrganizationBundle\Form\Type\EmployeeHistoryType;
use BooleanLogics\OrganizationBundle\Form\Type\EmployeeEducationType;
use BooleanLogics\OrganizationBundle\Form\Type\EmployeeCertificationType;
use BooleanLogics\OrganizationBundle\Form\Type\EmployeeJobHistoryType;
use BooleanLogics\CoreBundle\Service\StringHelper;
use BooleanLogics\CoreBundle\Service\ResponseHelper;
use BooleanLogics\CoreBundle\Service\FormErrorsSerializer;
use BooleanLogics\CoreBundle\Service\SystemMessage;
use BooleanLogics\CoreBundle\Model\DatatableListManager;

class EmployeeController extends Controller {

    private $systemMessage;
    private $response;

    public function __construct() {
        $this->systemMessage = new SystemMessage();
        $this->response = new ResponseHelper();
    }

    /**
     * @Route("/employee/create", name="employee_new")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function newAction(request $request) {
        $employee = new Employee();

        $organization = $this->container->get('organization_manager')->getOrganization();

        $form = $this->createForm(EmployeeType::class, $employee, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('employee_new'),
            'organization' => $organization
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($employee);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Employee', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }

        return $this->render('BooleanLogicsOrganizationBundle:Employee:new.html.twig', array(
                    'form' => $form->createView(),
                    'organization' => $organization,
                    'employee' => $employee,
        ));
    }

    /**
     * @Route("/employee/detail/create/{employee}", name="add_employee_detail_form")
     * @Template()
     * @ParamConverter("employee",class="BooleanLogicsOrganizationBundle:Employee")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function employeeDetaliFormAction(request $request, Employee $employee) {

        $employeeHistory = new EmployeeHistory();

        $organization = $this->container->get('organization_manager')->getOrganization();

        $form = $this->createForm(EmployeeHistoryType::class, $employeeHistory, array(
            'validation_groups' => array('create', 'Default'),
            'organization' => $organization,
            'action' => $this->generateUrl('add_employee_detail', array(
                'employee' => $employee->getId(),
            ))
        ));

        $form->handleRequest($request);

        $template = 'BooleanLogicsOrganizationBundle:Employee:add-employee-detail.html.twig';
        if ($request->isXmlHttpRequest()) { //AJAX request
            $template = 'BooleanLogicsOrganizationBundle:Employee:employee-detail-form-template.html.twig';
        }
        return $this->render($template, array(
                    'form' => $form->createView(),
                    'organization' => $organization,
                    'employee' => $employee,
        ));
    }

    /**
     * @Route("/employee/detail/{employee}", name="add_employee_detail")
     * @Template()
     * @ParamConverter("employee",class="BooleanLogicsOrganizationBundle:Employee")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function addEmployeeDetailAction(request $request, Employee $employee) {
        $employeeHistory = new EmployeeHistory();
        $organization = $this->container->get('organization_manager')->getOrganization();
        $form = $this->createForm(EmployeeHistoryType::class, $employeeHistory, array(
            'validation_groups' => array('create', 'Default'),
            'organization' => $organization,
            'action' => $this->generateUrl('add_employee_detail', array(
                'employee' => $employee->getId(),
            ))
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $employeeHistory->setCreator($this->getUser());
                $employeeHistory->setOrgnization($organization);
                $employeeHistory->setEmployee($employee);
                $em = $this->getDoctrine()->getManager();
                $em->persist($employeeHistory);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Employee Detail', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return array('form' => $form->createView());
    }

    /**
     * @Route("/employee/add/education/{employee}", name="add_employee_eduction")
     * @Template()
     * @ParamConverter("employee",class="BooleanLogicsOrganizationBundle:Employee")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function addEducationAction(request $request, Employee $employee) {
        $mployeeEducation = new EmployeeEducation();

        $form = $this->createForm(EmployeeEducationType::class, $mployeeEducation, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('add_employee_eduction', array(
                'employee' => $employee->getId(),
            ))
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $mployeeEducation->setEmployee($employee);
                $em = $this->getDoctrine()->getManager();
                $em->persist($mployeeEducation);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Education', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return $this->render('BooleanLogicsOrganizationBundle:Employee:add-education.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/employee/add/certification/{employee}", name="add_employee_certification")
     * @Template()
     * @ParamConverter("employee",class="BooleanLogicsOrganizationBundle:Employee")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function addCertificationAction(request $request, Employee $employee) {
        $employeeCertificate = new EmployeeCertificate();

        $form = $this->createForm(EmployeeCertificationType::class, $employeeCertificate, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('add_employee_certification', array(
                'employee' => $employee->getId(),
            ))
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $employeeCertificate->setEmployee($employee);
                $em = $this->getDoctrine()->getManager();
                $em->persist($employeeCertificate);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Certificate', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return $this->render('BooleanLogicsOrganizationBundle:Employee:add-certificate.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/employee/job/history/add/{employee}", name="add_employee_job_history")
     * @Template()
     * @ParamConverter("employee",class="BooleanLogicsOrganizationBundle:Employee")
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function addJobHistoryAction(request $request, Employee $employee) {
        $employeeHistory = new EmployeeJobHistory();

        $form = $this->createForm(EmployeeJobHistoryType::class, $employeeHistory, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('add_employee_job_history', array(
                'employee' => $employee->getId(),
            ))
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $employeeHistory->setEmployee($employee);
                $em = $this->getDoctrine()->getManager();
                $em->persist($employeeHistory);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Job History', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return $this->render('BooleanLogicsOrganizationBundle:Employee:add-job-history.html.twig', array(
                    'form' => $form->createView()
        ));
    }

}
