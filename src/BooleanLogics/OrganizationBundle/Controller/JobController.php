<?php

namespace BooleanLogics\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use BooleanLogics\OrganizationBundle\Entity\Organization;
use BooleanLogics\OrganizationBundle\Entity\Job;
use BooleanLogics\OrganizationBundle\Form\Type\JobType;
use BooleanLogics\CoreBundle\Service\StringHelper;
use BooleanLogics\CoreBundle\Service\ResponseHelper;
use BooleanLogics\CoreBundle\Service\FormErrorsSerializer;
use BooleanLogics\CoreBundle\Service\SystemMessage;
use BooleanLogics\CoreBundle\Model\DatatableListManager;

class JobController extends Controller {

    private $systemMessage;
    private $response;

    public function __construct() {
        $this->systemMessage = new SystemMessage();
        $this->response = new ResponseHelper();
    }

    /**
     * @Route("/job/create", name="job_new")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function newAction(request $request) {

        $job = new Job();
        $organization = $this->container->get('organization_manager')->getOrganization();

        $form = $this->createForm(JobType::class, $job, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('job_new_save'),
            'organization' => $organization
        ));

        $form->handleRequest($request);
        $template = 'BooleanLogicsOrganizationBundle:Job:new.html.twig';
        if ($request->isXmlHttpRequest()) { //AJAX request
            $template = 'BooleanLogicsOrganizationBundle:Job:new-form-template.html.twig';
        }
        return $this->render($template, array(
                    'form' => $form->createView(),
                    'organization' => $organization
        ));
    }

    /**
     * @Route("/job/save", name="job_new_save")
     * @Template()
     * @Security("is_granted('ROLE_ADMIN') and is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Method({"GET","POST"})
     */
    public function postSaveAction(request $request) {
        $job = new Job();
        $organization = $this->container->get('organization_manager')->getOrganization();
        $form = $this->createForm(JobType::class, $job, array(
            'validation_groups' => array('create', 'Default'),
            'action' => $this->generateUrl('job_new_save'),
            'organization' => $organization
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if ($form->isValid()) { //Form data validate
                $formDate = $form->getData();
                $job->setCreator($this->getUser());
                $job->setOrgnization($organization);
                $em = $this->getDoctrine()->getManager();
                $em->persist($job);
                $em->flush();
                return $this->response->getResponse([
                            'Good' => true,
                            'Success' => $this->systemMessage->getMessage('Job', 'success'),
                ]);
            } else {
                $errors = new FormErrorsSerializer();
                if ($request->isXmlHttpRequest()) { //AJAX request
                    return $this->response->getResponse([
                                'Good' => false,
                                'Errors' => $errors->serializeFormErrors($form, true, false),
                    ]);
                }
            }
        }
        return array('form' => $form->createView());
    }

}
