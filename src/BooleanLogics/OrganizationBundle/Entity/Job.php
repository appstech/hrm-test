<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Job
 *
 * @ORM\Table(name="blt_job")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\JobRepository")
 * @UniqueEntity("title")
 * @ORM\HasLifecycleCallbacks()
 */
class Job extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\CoreBundle\Entity\User", inversedBy="jobs")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Organization", inversedBy="jobs")
     * @ORM\JoinColumn(name="orgnization_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $orgnization;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Branch", inversedBy="jobs")
     * @ORM\JoinColumn(name="branch_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     * @Assert\NotBlank(message = "Branch not blank")
     */
    private $branch;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Department", inversedBy="jobs")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     * @Assert\NotBlank(message = "Department not blank")
     */
    private $department;

    /**
     * @var string
     * @Assert\NotBlank(message = "Title not blank")
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message = "Description not blank")
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     * @Assert\NotBlank(message = "Min Salary not blank")
     * @ORM\Column(name="min_salary", type="integer", nullable=true,options={"default"=0})
     */
    private $minSalary;

    /**
     * @var string
     * @Assert\NotBlank(message = "Max Salary not blank")
     * @ORM\Column(name="max_salary", type="integer", nullable=true,options={"default"=0})
     */
    private $maxSalary;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Job
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Job
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set minSalary
     *
     * @param integer $minSalary
     *
     * @return Job
     */
    public function setMinSalary($minSalary) {
        $this->minSalary = $minSalary;

        return $this;
    }

    /**
     * Get minSalary
     *
     * @return integer
     */
    public function getMinSalary() {
        return $this->minSalary;
    }

    /**
     * Set maxSalary
     *
     * @param integer $maxSalary
     *
     * @return Job
     */
    public function setMaxSalary($maxSalary) {
        $this->maxSalary = $maxSalary;

        return $this;
    }

    /**
     * Get maxSalary
     *
     * @return integer
     */
    public function getMaxSalary() {
        return $this->maxSalary;
    }

    /**
     * Set orgnization
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Organization $orgnization
     *
     * @return Job
     */
    public function setOrgnization(\BooleanLogics\OrganizationBundle\Entity\Organization $orgnization = null) {
        $this->orgnization = $orgnization;

        return $this;
    }

    /**
     * Get orgnization
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Organization
     */
    public function getOrgnization() {
        return $this->orgnization;
    }

    /**
     * Set creator
     *
     * @param \BooleanLogics\CoreBundle\Entity\User $creator
     *
     * @return Job
     */
    public function setCreator(\BooleanLogics\CoreBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \BooleanLogics\CoreBundle\Entity\User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Set branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     *
     * @return Job
     */
    public function setBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch = null) {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Branch
     */
    public function getBranch() {
        return $this->branch;
    }

    /**
     * Set department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     *
     * @return Job
     */
    public function setDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department = null) {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Department
     */
    public function getDepartment() {
        return $this->department;
    }

}
