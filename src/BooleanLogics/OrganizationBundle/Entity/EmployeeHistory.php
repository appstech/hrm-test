<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * EmployeeHistory
 *
 * @ORM\Table(name="blt_employee_history")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\EmployeeHistoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EmployeeHistory extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Employee", inversedBy="histories")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $employee;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\CoreBundle\Entity\User", inversedBy="employees")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Organization", inversedBy="employees")
     * @ORM\JoinColumn(name="orgnization_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $orgnization;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Branch", inversedBy="employees")
     * @ORM\JoinColumn(name="branch_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $branch;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Department", inversedBy="employees")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Designation", inversedBy="employees")
     * @ORM\JoinColumn(name="designation_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $designation;

    /**
     * @var string
     * @Assert\NotBlank(message = "Salary not blank")
     * @ORM\Column(name="salary", type="integer", nullable=true,options={"default"=0})
     */
    private $salary;

    /**
     * @var string
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var string
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set salary
     *
     * @param integer $salary
     *
     * @return EmployeeHistory
     */
    public function setSalary($salary) {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return integer
     */
    public function getSalary() {
        return $this->salary;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return EmployeeHistory
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return EmployeeHistory
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Set employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Employee $employee
     *
     * @return EmployeeHistory
     */
    public function setEmployee(\BooleanLogics\OrganizationBundle\Entity\Employee $employee = null) {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Employee
     */
    public function getEmployee() {
        return $this->employee;
    }

    /**
     * Set creator
     *
     * @param \BooleanLogics\CoreBundle\Entity\User $creator
     *
     * @return EmployeeHistory
     */
    public function setCreator(\BooleanLogics\CoreBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \BooleanLogics\CoreBundle\Entity\User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Set orgnization
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Organization $orgnization
     *
     * @return EmployeeHistory
     */
    public function setOrgnization(\BooleanLogics\OrganizationBundle\Entity\Organization $orgnization = null) {
        $this->orgnization = $orgnization;

        return $this;
    }

    /**
     * Get orgnization
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Organization
     */
    public function getOrgnization() {
        return $this->orgnization;
    }

    /**
     * Set branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     *
     * @return EmployeeHistory
     */
    public function setBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch = null) {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Branch
     */
    public function getBranch() {
        return $this->branch;
    }

    /**
     * Set department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     *
     * @return EmployeeHistory
     */
    public function setDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department = null) {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Department
     */
    public function getDepartment() {
        return $this->department;
    }

    /**
     * Set designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     *
     * @return EmployeeHistory
     */
    public function setDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation = null) {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Designation
     */
    public function getDesignation() {
        return $this->designation;
    }

}
