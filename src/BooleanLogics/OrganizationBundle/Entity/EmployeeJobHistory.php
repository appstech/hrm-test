<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * EmployeeJobHistory
 *
 * @ORM\Table(name="blt_employee_job_history")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\EmployeeJobHistoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EmployeeJobHistory extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="sort_order", type="integer", nullable=true, options={"default"=0})
     */
    private $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Employee", inversedBy="employements")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $employee;

    /**
     * @var string
     * @Assert\NotBlank(message = "Degree title not blank")
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message = "Company not blank")
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var string
     * @Assert\NotBlank(message = "Join Date not blank")
     * @ORM\Column(name="join_date", type="datetime", nullable=true)
     */
    private $joinDate;

    /**
     * @var string
     * @Assert\NotBlank(message = "Resign Date not blank")
     * @ORM\Column(name="resign_date", type="datetime", nullable=true)
     */
    private $resignDate;

    /**
     * @var string
     * @Assert\NotBlank(message = "Description not blank")
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return EmployeeJobHistory
     */
    public function setSortOrder($sortOrder) {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder() {
        return $this->sortOrder;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return EmployeeJobHistory
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return EmployeeJobHistory
     */
    public function setCompany($company) {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     * Set joinDate
     *
     * @param \DateTime $joinDate
     *
     * @return EmployeeJobHistory
     */
    public function setJoinDate($joinDate) {
        $this->joinDate = $joinDate;

        return $this;
    }

    /**
     * Get joinDate
     *
     * @return \DateTime
     */
    public function getJoinDate() {
        return $this->joinDate;
    }

    /**
     * Set resignDate
     *
     * @param \DateTime $resignDate
     *
     * @return EmployeeJobHistory
     */
    public function setResignDate($resignDate) {
        $this->resignDate = $resignDate;

        return $this;
    }

    /**
     * Get resignDate
     *
     * @return \DateTime
     */
    public function getResignDate() {
        return $this->resignDate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return EmployeeJobHistory
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Employee $employee
     *
     * @return EmployeeJobHistory
     */
    public function setEmployee(\BooleanLogics\OrganizationBundle\Entity\Employee $employee = null) {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Employee
     */
    public function getEmployee() {
        return $this->employee;
    }

}
