<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Organization
 *
 * @ORM\Table(name="blt_organization")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\OrganizationRepository")
 * @UniqueEntity("name")
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks()
 */
class Organization extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Branch", mappedBy="orgnization")
     */
    private $branches;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Department", mappedBy="orgnization")
     */
    private $departments;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Designation", mappedBy="orgnization")
     */
    private $designations;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\CoreBundle\Entity\User", inversedBy="organizations")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creator;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Job", mappedBy="orgnization")
     */
    private $jobs;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="orgnization")
     */
    private $employees;

    /**
     * @var string
     * @Assert\NotBlank(message = "Name not blank")
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="unique_id", type="string", length=255, unique=true)
     */
    private $unique;

    /**
     * @var string
     * @Assert\NotBlank(message = "Email not blank")
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="tag_line", type="text", nullable=true)
     */
    private $tagline;

    /**
     * @var string
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var datetime
     *
     * @ORM\Column(name="started_at", type="datetime",nullable=true)
     */
    protected $startedAt;

    /**
     * @var string
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage = "Please upload a valid JPG,JPEG,PNG"
     * )
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var string
     * @ORM\Column(name="registeration_number", type="string", length=65, nullable=true)
     */
    private $registerationNumber;

    /**
     * @var string
     * @ORM\Column(name="national_tax_number", type="string", length=65, nullable=true)
     */
    private $nationalTaxNumber;

    /**
     * @var string
     * @ORM\Column(name="vision", type="text", nullable=true)
     */
    private $vision;

    /**
     * @var string
     * @ORM\Column(name="mission", type="text", nullable=true)
     */
    private $mission;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Organization
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Organization
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set tagline
     *
     * @param string $tagline
     *
     * @return Organization
     */
    public function setTagline($tagline) {
        $this->tagline = $tagline;

        return $this;
    }

    /**
     * Get tagline
     *
     * @return string
     */
    public function getTagline() {
        return $this->tagline;
    }

    /**
     * Set startedAt
     *
     * @param \DateTime $startedAt
     *
     * @return Organization
     */
    public function setStartedAt($startedAt) {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Get startedAt
     *
     * @return \DateTime
     */
    public function getStartedAt() {
        return $this->startedAt;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Organization
     */
    public function setLogo($logo) {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo() {
        return $this->logo;
    }

    /**
     * Set registerationNumber
     *
     * @param string $registerationNumber
     *
     * @return Organization
     */
    public function setRegisterationNumber($registerationNumber) {
        $this->registerationNumber = $registerationNumber;

        return $this;
    }

    /**
     * Get registerationNumber
     *
     * @return string
     */
    public function getRegisterationNumber() {
        return $this->registerationNumber;
    }

    /**
     * Set nationalTaxNumber
     *
     * @param string $nationalTaxNumber
     *
     * @return Organization
     */
    public function setNationalTaxNumber($nationalTaxNumber) {
        $this->nationalTaxNumber = $nationalTaxNumber;

        return $this;
    }

    /**
     * Get nationalTaxNumber
     *
     * @return string
     */
    public function getNationalTaxNumber() {
        return $this->nationalTaxNumber;
    }

    /**
     * Set vision
     *
     * @param string $vision
     *
     * @return Organization
     */
    public function setVision($vision) {
        $this->vision = $vision;

        return $this;
    }

    /**
     * Get vision
     *
     * @return string
     */
    public function getVision() {
        return $this->vision;
    }

    /**
     * Set mission
     *
     * @param string $mission
     *
     * @return Organization
     */
    public function setMission($mission) {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission
     *
     * @return string
     */
    public function getMission() {
        return $this->mission;
    }

    /**
     * Set creator
     *
     * @param \BooleanLogics\CoreBundle\Entity\User $creator
     *
     * @return Organization
     */
    public function setCreator(\BooleanLogics\CoreBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \BooleanLogics\CoreBundle\Entity\User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Set unique
     *
     * @param string $unique
     *
     * @return Organization
     */
    public function setUnique($unique) {
        $this->unique = $unique;

        return $this;
    }

    /**
     * Get unique
     *
     * @return string
     */
    public function getUnique() {
        return $this->unique;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Organization
     */
    public function setUrl($url) {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->branches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     *
     * @return Organization
     */
    public function addBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch) {
        $this->branches[] = $branch;

        return $this;
    }

    /**
     * Remove branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     */
    public function removeBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch) {
        $this->branches->removeElement($branch);
    }

    /**
     * Get branches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBranches() {
        return $this->branches;
    }

    /**
     * Add department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     *
     * @return Organization
     */
    public function addDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department) {
        $this->departments[] = $department;

        return $this;
    }

    /**
     * Remove department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     */
    public function removeDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department) {
        $this->departments->removeElement($department);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartments() {
        return $this->departments;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Organization
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Add designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     *
     * @return Organization
     */
    public function addDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations[] = $designation;

        return $this;
    }

    /**
     * Remove designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     */
    public function removeDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations->removeElement($designation);
    }

    /**
     * Get designations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignations() {
        return $this->designations;
    }

    /**
     * Add job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     *
     * @return Organization
     */
    public function addJob(\BooleanLogics\OrganizationBundle\Entity\Job $job) {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     */
    public function removeJob(\BooleanLogics\OrganizationBundle\Entity\Job $job) {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs() {
        return $this->jobs;
    }


    /**
     * Add employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     *
     * @return Organization
     */
    public function addEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     */
    public function removeEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }
}
