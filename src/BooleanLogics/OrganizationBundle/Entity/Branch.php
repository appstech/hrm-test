<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Branch
 *
 * @ORM\Table(name="blt_branch")
 * @UniqueEntity("name")
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\BranchRepository")
 */
class Branch extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\CoreBundle\Entity\User", inversedBy="branches")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Organization", inversedBy="branches")
     * @ORM\JoinColumn(name="orgnization_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $orgnization;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Department", mappedBy="branch")
     */
    private $departments;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Designation", mappedBy="branch")
     */
    private $designations;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Job", mappedBy="department")
     */
    private $jobs;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="branch")
     */
    private $employees;

    /**
     * @var string
     * @Assert\NotBlank(message = "Name not blank")
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     * @Assert\NotBlank(message = "Email not blank")
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     */
    private $zipcode;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     */
    private $longitude;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Branch
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Branch
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Branch
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Branch
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Branch
     */
    public function setFax($fax) {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax() {
        return $this->fax;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Branch
     */
    public function setMobile($mobile) {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile() {
        return $this->mobile;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Branch
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Branch
     */
    public function setState($state) {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState() {
        return $this->state;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return Branch
     */
    public function setZipcode($zipcode) {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode() {
        return $this->zipcode;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Branch
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set creator
     *
     * @param \BooleanLogics\CoreBundle\Entity\User $creator
     *
     * @return Branch
     */
    public function setCreator(\BooleanLogics\CoreBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \BooleanLogics\CoreBundle\Entity\User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Set orgnization
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Organization $orgnization
     *
     * @return Branch
     */
    public function setOrgnization(\BooleanLogics\OrganizationBundle\Entity\Organization $orgnization = null) {
        $this->orgnization = $orgnization;

        return $this;
    }

    /**
     * Get orgnization
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Organization
     */
    public function getOrgnization() {
        return $this->orgnization;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Branch
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Branch
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->departments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     *
     * @return Branch
     */
    public function addDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department) {
        $this->departments[] = $department;

        return $this;
    }

    /**
     * Remove department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     */
    public function removeDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department) {
        $this->departments->removeElement($department);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartments($flag = false) {
//        $criteria = \Doctrine\Common\Collections\Criteria::create();
//        if ($flag !== true) {
//            $criteria->where(\Doctrine\Common\Collections\Criteria::expr()->eq('status', '0'));
//        }
//        return $this->departments->matching($criteria);
        return $this->departments;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Branch
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Add designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     *
     * @return Branch
     */
    public function addDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations[] = $designation;

        return $this;
    }

    /**
     * Remove designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     */
    public function removeDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations->removeElement($designation);
    }

    /**
     * Get designations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignations() {
        return $this->designations;
    }

    /**
     * Add job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     *
     * @return Branch
     */
    public function addJob(\BooleanLogics\OrganizationBundle\Entity\Job $job) {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     */
    public function removeJob(\BooleanLogics\OrganizationBundle\Entity\Job $job) {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs() {
        return $this->jobs;
    }


    /**
     * Add employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     *
     * @return Branch
     */
    public function addEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     */
    public function removeEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }
}
