<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Designation
 *
 * @ORM\Table(name="blt_designation")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\DesignationRepository")
 * @UniqueEntity("name")
 * @ORM\HasLifecycleCallbacks()
 */
class Designation extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message = "Name not blank")
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\CoreBundle\Entity\User", inversedBy="designations")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Organization", inversedBy="designations")
     * @ORM\JoinColumn(name="orgnization_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $orgnization;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Branch", inversedBy="designations")
     * @ORM\JoinColumn(name="branch_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     * @Assert\NotBlank(message = "Branch not blank")
     */
    private $branch;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Department", inversedBy="designations")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     * @Assert\NotBlank(message = "Department not blank")
     */
    private $department;
    
    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="designation")
     */
    private $employees;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Designation
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set creator
     *
     * @param \BooleanLogics\CoreBundle\Entity\User $creator
     *
     * @return Designation
     */
    public function setCreator(\BooleanLogics\CoreBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \BooleanLogics\CoreBundle\Entity\User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Set orgnization
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Organization $orgnization
     *
     * @return Designation
     */
    public function setOrgnization(\BooleanLogics\OrganizationBundle\Entity\Organization $orgnization = null) {
        $this->orgnization = $orgnization;

        return $this;
    }

    /**
     * Get orgnization
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Organization
     */
    public function getOrgnization() {
        return $this->orgnization;
    }

    /**
     * Set branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     *
     * @return Designation
     */
    public function setBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch = null) {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Branch
     */
    public function getBranch() {
        return $this->branch;
    }

    /**
     * Set department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     *
     * @return Designation
     */
    public function setDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department = null) {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Department
     */
    public function getDepartment() {
        return $this->department;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     *
     * @return Designation
     */
    public function addEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     */
    public function removeEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }
}
