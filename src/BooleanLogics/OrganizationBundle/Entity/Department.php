<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Department
 *
 * @ORM\Table(name="blt_department")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\DepartmentRepository")
 * @UniqueEntity("name")
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks()
 */
class Department extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\CoreBundle\Entity\User", inversedBy="departments")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Organization", inversedBy="departments")
     * @ORM\JoinColumn(name="orgnization_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $orgnization;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Branch", inversedBy="departments")
     * @ORM\JoinColumn(name="branch_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $branch;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Designation", mappedBy="designation")
     */
    private $designations;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Job", mappedBy="department")
     */
    private $jobs;
    
    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="department")
     */
    private $employees;

    /**
     * @var string
     * @Assert\NotBlank(message = "Name not blank")
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     * @Assert\NotBlank(message = "Email not blank")
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Department
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Department
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Department
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set orgnization
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Organization $orgnization
     *
     * @return Department
     */
    public function setOrgnization(\BooleanLogics\OrganizationBundle\Entity\Organization $orgnization = null) {
        $this->orgnization = $orgnization;

        return $this;
    }

    /**
     * Get orgnization
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Organization
     */
    public function getOrgnization() {
        return $this->orgnization;
    }

    /**
     * Set branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     *
     * @return Department
     */
    public function setBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch = null) {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Branch
     */
    public function getBranch() {
        return $this->branch;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Department
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set creator
     *
     * @param \BooleanLogics\CoreBundle\Entity\User $creator
     *
     * @return Department
     */
    public function setCreator(\BooleanLogics\CoreBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \BooleanLogics\CoreBundle\Entity\User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->designations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     *
     * @return Department
     */
    public function addDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations[] = $designation;

        return $this;
    }

    /**
     * Remove designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     */
    public function removeDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations->removeElement($designation);
    }

    /**
     * Get designations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignations() {
        return $this->designations;
    }

    /**
     * Add job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     *
     * @return Department
     */
    public function addJob(\BooleanLogics\OrganizationBundle\Entity\Job $job) {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     */
    public function removeJob(\BooleanLogics\OrganizationBundle\Entity\Job $job) {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs() {
        return $this->jobs;
    }


    /**
     * Add employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     *
     * @return Department
     */
    public function addEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     */
    public function removeEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }
}
