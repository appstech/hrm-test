<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * EmployeeEducation
 *
 * @ORM\Table(name="blt_employee_education")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\EmployeeEducationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EmployeeEducation extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="sort_order", type="integer", nullable=true, options={"default"=0})
     */
    private $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="BooleanLogics\OrganizationBundle\Entity\Employee", inversedBy="educations")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $employee;

    /**
     * @var string
     * @Assert\NotBlank(message = "Degree title not blank")
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message = "Institute not blank")
     * @ORM\Column(name="institute", type="string", length=255, nullable=true)
     */
    private $institute;

    /**
     * @var string
     * @Assert\NotBlank(message = "Start Date not blank")
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var string
     * @Assert\NotBlank(message = "End Date not blank")
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string
     * @Assert\NotBlank(message = "Description not blank")
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return EmployeeEducation
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return EmployeeEducation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set institute
     *
     * @param string $institute
     *
     * @return EmployeeEducation
     */
    public function setInstitute($institute)
    {
        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return string
     */
    public function getInstitute()
    {
        return $this->institute;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return EmployeeEducation
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return EmployeeEducation
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return EmployeeEducation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Employee $employee
     *
     * @return EmployeeEducation
     */
    public function setEmployee(\BooleanLogics\OrganizationBundle\Entity\Employee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \BooleanLogics\OrganizationBundle\Entity\Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }
}
