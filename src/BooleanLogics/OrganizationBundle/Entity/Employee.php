<?php

namespace BooleanLogics\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Employee
 *
 * @ORM\Table(name="blt_employee")
 * @ORM\Entity(repositoryClass="BooleanLogics\OrganizationBundle\Repository\EmployeeRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("email")
 */
class Employee extends \BooleanLogics\CoreBundle\Entity\AbstractEntity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="employee")
     */
    private $histories;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="employee")
     */
    private $educations;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="employee")
     */
    private $certificates;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="employee")
     */
    private $employements;

    /**
     * @var string
     * @Assert\NotBlank(message = "First name not blank")
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank(message = "Last name not blank")
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank(message = "Email not blank")
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="dob", type="datetime", nullable=true)
     */
    private $dob;

    /**
     * @var string
     * @ORM\Column(name="join_date", type="datetime", nullable=true)
     */
    private $joinDate;

    /**
     * @var string
     * @ORM\Column(name="age", type="integer", nullable=true, options={"default"=0})
     */
    private $age;

    /**
     * @var string
     * @ORM\Column(name="gender", type="integer", nullable=true, options={"default"=-1})
     */
    private $gender;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     */
    private $zipcode;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(name="remarks", type="text", nullable=true)
     */
    private $remarks;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Employee
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Employee
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Employee
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     *
     * @return Employee
     */
    public function setDob($dob) {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime
     */
    public function getDob() {
        return $this->dob;
    }

    /**
     * Set joinDate
     *
     * @return Employee
     */
    public function setJoinDate($joinDate) {
//        $dt = new \DateTime();
//        $dt->setTimezone(new \DateTimeZone('UTC'));
//        $this->joinDate = $dt;
        $this->joinDate = $joinDate;
        return $this;
    }

    /**
     * Get joinDate
     *
     * @return \DateTime
     */
    public function getJoinDate() {
        return $this->joinDate;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return Employee
     */
    public function setAge($age) {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer
     */
    public function getAge() {
        return $this->age;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     *
     * @return Employee
     */
    public function setGender($gender) {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Employee
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Employee
     */
    public function setMobile($mobile) {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile() {
        return $this->mobile;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Employee
     */
    public function setState($state) {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState() {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Employee
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return Employee
     */
    public function setZipcode($zipcode) {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode() {
        return $this->zipcode;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Employee
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return Employee
     */
    public function setRemarks($remarks) {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks() {
        return $this->remarks;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add history
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $history
     *
     * @return Employee
     */
    public function addHistory(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $history) {
        $this->histories[] = $history;

        return $this;
    }

    /**
     * Remove history
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $history
     */
    public function removeHistory(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $history) {
        $this->histories->removeElement($history);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistories() {
        return $this->histories;
    }

    /**
     * Add education
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $education
     *
     * @return Employee
     */
    public function addEducation(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $education) {
        $this->educations[] = $education;

        return $this;
    }

    /**
     * Remove education
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $education
     */
    public function removeEducation(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $education) {
        $this->educations->removeElement($education);
    }

    /**
     * Get educations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEducations() {
        return $this->educations;
    }

    /**
     * Add certificate
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $certificate
     *
     * @return Employee
     */
    public function addCertificate(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $certificate) {
        $this->certificates[] = $certificate;

        return $this;
    }

    /**
     * Remove certificate
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $certificate
     */
    public function removeCertificate(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $certificate) {
        $this->certificates->removeElement($certificate);
    }

    /**
     * Get certificates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCertificates() {
        return $this->certificates;
    }

    /**
     * Add employement
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employement
     *
     * @return Employee
     */
    public function addEmployement(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employement) {
        $this->employements[] = $employement;

        return $this;
    }

    /**
     * Remove employement
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employement
     */
    public function removeEmployement(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employement) {
        $this->employements->removeElement($employement);
    }

    /**
     * Get employements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployements() {
        return $this->employements;
    }

}
