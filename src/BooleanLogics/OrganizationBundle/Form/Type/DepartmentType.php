<?php

namespace BooleanLogics\OrganizationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BooleanLogics\CoreBundle\Model\Status;

class DepartmentType extends AbstractType {

    private $orgnization;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $this->orgnization = $options['organization'];

        $builder
                ->add('name', TextType::class, array('required' => true))
                ->add('email', EmailType::class, array('required' => true))
                ->add('branch', EntityType::class, array(
                    'class' => 'BooleanLogics\OrganizationBundle\Entity\Branch',
                    'placeholder' => 'Select',
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                                ->where("a.status = '" . Status::Active . "'")
                                ->andWhere("a.orgnization = '" . $this->orgnization->getId() . "'")
                                ->orderBy('a.name', 'ASC');
                    },
                    'required' => true,
                ))
                ->add('status', ChoiceType::class, array(
                    'choices' => array(Status::ShowActive => Status::Active, Status::ShowInActive => Status::InActive),
                    'required' => true,
        ));
        $builder->add('Create', ButtonType::class);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BooleanLogics\OrganizationBundle\Entity\Department',
            'cascade_validation' => true
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'organization' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'booleanlogics_organizationbundle_department';
    }

}
