<?php

namespace BooleanLogics\OrganizationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BooleanLogics\CoreBundle\Model\Status;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class JobType extends AbstractType {

    private $orgnization;
    private $orgnizationId;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->orgnization = $options['organization'];
        $this->orgnizationId = $this->orgnization->getId();

        $builder
                ->add('title', TextType::class, array('required' => true))
                ->add('branch', EntityType::class, array(
                    'class' => 'BooleanLogics\OrganizationBundle\Entity\Branch',
                    'placeholder' => 'Selelct',
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                                ->where("a.status = '" . Status::Active . "'")
                                ->andWhere("a.orgnization = '" . $this->orgnizationId . "'")
                                ->orderBy('a.name', 'ASC');
                    },
                    'required' => true,
                ))
                ->add('minSalary', NumberType::class, array('required' => true))
                ->add('maxSalary', NumberType::class, array('required' => true))
                ->add('description', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array('required' => true))
                ->add('status', ChoiceType::class, array(
                    'choices' => array(Status::ShowActive => Status::Active, Status::ShowInActive => Status::InActive),
                    'required' => true,
        ));


        $formModifier = function (\Symfony\Component\Form\FormInterface $form,
                \BooleanLogics\OrganizationBundle\Entity\Branch $branch = null) {
            $positions = null === $branch ? array() : $branch->getDepartments(true);

            $placeholder = 'Select';
            $class = 'BooleanLogicsOrganizationBundle:Department';

            if ($branch) {
                $form->add('department', EntityType::class, array(
                    'class' => $class,
                    'placeholder' => $placeholder,
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) use ($branch) {
                        return $er->createQueryBuilder('d')
                                        ->where("d.status = '" . Status::Active . "'")
                                        ->andWhere("d.branch=:branch")
                                        ->setParameter('branch', $branch->getId())
                                        ->orderBy('d.name', 'ASC');
                    },
                    'choices_as_values' => true,
                ));
            } else {
                $form->add('department', EntityType::class, array(
                    'class' => $class,
                    'placeholder' => '1231',
                    'choices' => $positions,
                    'choice_label' => function($positions) {
                        return $positions->getName();
                    },
                    'choices_as_values' => true,
                ));
            }
        };
        $builder->addEventListener(//when department init
                FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formModifier) {
            $data = $event->getData();
            $formModifier($event->getForm(), $data->getDepartment());
        }
        );

        $builder->get('branch')->addEventListener(//when branch change get departments
                FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formModifier) {
            $sport = $event->getForm()->getData();
            $formModifier($event->getForm()->getParent(), $sport);
        }
        );

        $builder->add('Create', ButtonType::class);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BooleanLogics\OrganizationBundle\Entity\Job',
            'cascade_validation' => true
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'organization' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'booleanlogics_organizationbundle_job';
    }

}
