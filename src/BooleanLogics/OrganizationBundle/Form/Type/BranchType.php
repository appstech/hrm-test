<?php

namespace BooleanLogics\OrganizationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BooleanLogics\CoreBundle\Model\Status;

class BranchType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('name', TextType::class, array('required' => true))
                ->add('email', EmailType::class, array('required' => true))
                ->add('phone', TextType::class)
                ->add('mobile', TextType::class)
                ->add('fax', TextType::class)
                ->add('city', TextType::class)
                ->add('state', TextType::class)
                ->add('zipcode', TextType::class)
                ->add('address', TextType::class)
                ->add('latitude', TextType::class)
                ->add('longitude', TextType::class)
                ->add('status', ChoiceType::class, array(
                    'choices' => array(Status::ShowActive => Status::Active, Status::ShowInActive => Status::InActive),
                    'required' => true,
        ));
        $builder->add('Create', ButtonType::class);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BooleanLogics\OrganizationBundle\Entity\Branch',
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'booleanlogics_organizationbundle_branch';
    }

}
