<?php

namespace BooleanLogics\OrganizationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use BooleanLogics\CoreBundle\Model\Status;

class EmployeeJobHistoryType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('title', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The title is required.']),
                    ],
                ))
                ->add('company', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The company is required.']),
                    ],
                ))
                ->add('joinDate', DateType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The join date is required.']),
                    ],
                ))
                ->add('resignDate', DateType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The resign date is required.']),
                    ],
                ))
                ->add('description', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class)
                ->add('status', ChoiceType::class, array(
                    'choices' => array(
                        Status::ShowActive => Status::Active,
                        Status::ShowInActive => Status::InActive
                    ),
                    'required' => true,
        ));
        $builder->add('Create', ButtonType::class);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BooleanLogics\OrganizationBundle\Entity\EmployeeJobHistory',
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'booleanlogics_organizationbundle_employee_job_history';
    }

}
