<?php

namespace BooleanLogics\OrganizationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BooleanLogics\CoreBundle\Model\Status;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EmployeeHistoryType extends AbstractType {

    private $orgnization;
    private $orgnizationId;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $this->orgnization = $options['organization'];
        $this->orgnizationId = $this->orgnization->getId();

        $builder
                ->add('branch', EntityType::class, array(
                    'class' => 'BooleanLogics\OrganizationBundle\Entity\Branch',
                    'choice_label' => 'name',
                    'placeholder' => 'Select',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                                ->where("a.status = '" . Status::Active . "'")
                                ->andWhere("a.orgnization = '" . $this->orgnizationId . "'")
                                ->orderBy('a.name', 'ASC');
                    },
                    'required' => true,
                ))
                ->add('department', EntityType::class, array(
                    'class' => 'BooleanLogicsOrganizationBundle:Department',
                    'placeholder' => 'Select',
                    'choices_as_values' => true,
                ))
                ->add('designation', EntityType::class, array(
                    'class' => 'BooleanLogicsOrganizationBundle:Designation',
                    'placeholder' => 'Select',
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                                ->where("d.status = '" . Status::Active . "'")
                                //->andWhere("d.orgnization = '" . $this->orgnizationId . "'")
                                //->andWhere("d.department=:department")
                                //->setParameter('department', $department->getId())
                                ->orderBy('d.name', 'ASC');
                    },
                    'choices_as_values' => true,
                ))
                ->add('salary', NumberType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The salary is required.']),
                    ],
                ))
                ->add('startDate', DateType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The start date is required.']),
                    ],
                ))
                ->add('endDate', DateType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The end date is required.']),
                    ],
                ))
                ->add('status', ChoiceType::class, array(
                    'choices' => array(Status::ShowActive => Status::Active, Status::ShowInActive => Status::InActive),
                    'required' => true,
        ));

        $builder->addEventListener(//when department init
                FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $this->getDeprtmentByBranch($event->getForm(), $data->getDepartment());
            //$this->getDesignationByDeprtment($event->getForm(), $data->getDesignation());
        }
        );

//        $builder->get('department')->addEventListener(//when department change get designations
//                FormEvents::POST_SUBMIT, function (FormEvent $event) {
//            $sport = $event->getForm()->getData();
//            $this->getDesignationByDeprtment($event->getForm()->getParent(), $sport);
//        }
//        );

        $builder->get('branch')->addEventListener(//when branch change get departments
                FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $sport = $event->getForm()->getData();
            $this->getDeprtmentByBranch($event->getForm()->getParent(), $sport);
        }
        );



        $builder->add('Create', ButtonType::class);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BooleanLogics\OrganizationBundle\Entity\EmployeeHistory',
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'booleanlogics_organizationbundle_employee_history';
    }

    private function getDeprtmentByBranch(\Symfony\Component\Form\FormInterface $form, \BooleanLogics\OrganizationBundle\Entity\Branch $branch = null) {
        $positions = null === $branch ? array() : $branch->getDepartments(true);

        $placeholder = 'Select';
        $class = 'BooleanLogicsOrganizationBundle:Department';

        if ($branch) {
            $form->add('department', EntityType::class, array(
                'class' => $class,
                'placeholder' => $placeholder,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) use ($branch) {
                    return $er->createQueryBuilder('d')
                                    ->where("d.status = '" . Status::Active . "'")
                                    ->andWhere("d.orgnization = '" . $this->orgnizationId . "'")
                                    ->andWhere("d.branch=:branch")
                                    ->setParameter('branch', $branch->getId())
                                    ->orderBy('d.name', 'ASC');
                },
                'choices_as_values' => true,
            ));
        } else {
            $form->add('department', EntityType::class, array(
                'class' => $class,
                'placeholder' => $placeholder,
                'choices' => $positions,
                'choice_label' => function($positions) {
                    return $positions->getName();
                },
                'choices_as_values' => true,
            ));
        }
    }

    private function getDesignationByDeprtment(\Symfony\Component\Form\FormInterface $form, \BooleanLogics\OrganizationBundle\Entity\Department $department = null) {
        $positions = null === $department ? array() : $department->getDesignations();


        $placeholder = 'Select';
        $class = 'BooleanLogicsOrganizationBundle:Designation';

        if ($department) {
            $form->add('designation', EntityType::class, array(
                'class' => $class,
                'placeholder' => $placeholder,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) use ($department) {
                    return $er->createQueryBuilder('d')
                                    ->where("d.status = '" . Status::Active . "'")
                                    //->andWhere("d.orgnization = '" . $this->orgnizationId . "'")
                                    //->andWhere("d.department=:department")
                                    //->setParameter('department', $department->getId())
                                    ->orderBy('d.name', 'ASC');
                },
                'choices_as_values' => true,
            ));
        } else {
            $form->add('designation', EntityType::class, array(
                'class' => $class,
                'placeholder' => $placeholder,
                'choices' => $positions,
                'choice_label' => function($positions) {
                    return $positions->getName();
                },
                'choices_as_values' => true,
            ));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'organization' => null,
        ));
    }

}
