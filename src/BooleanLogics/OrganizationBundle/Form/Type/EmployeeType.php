<?php

namespace BooleanLogics\OrganizationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BooleanLogics\CoreBundle\Model\Status;

class EmployeeType extends AbstractType {

    private $orgnization;
    private $orgnizationId;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $this->orgnization = $options['organization'];
        $this->orgnizationId = $this->orgnization->getId();

        $builder
                ->add('firstName', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The First Name is required.']),
                    ],
                ))
                ->add('lastName', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The last name is required.']),
                    ],
                ))
                ->add('dob', DateType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The date of birth is required.']),
                    ],
                ))
                ->add('email', EmailType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The email is required.']),
                        new Email(array('checkMX' => true)),
                    ],
                ))
                ->add('phone', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The phone is required.']),
                    ],
                ))
                ->add('mobile', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The mobile is required.']),
                    ],
                ))
                ->add('joinDate', DateType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The joining date is required.']),
                    ],
                ))
                ->add('age', NumberType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The age is required.']),
                    ],
                ))
                ->add('gender', ChoiceType::class, array(
                    'choices' => array(
                        'Male' => 1,
                        'FeMale' => 0
                    ),
                    'required' => true,
                ))
                ->add('city', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The city is required.']),
                    ],
                ))
                ->add('state', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The state is required.']),
                    ],
                ))
                ->add('zipcode', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The zipcode is required.']),
                    ],
                ))
                ->add('address', TextType::class, array(
                    'required' => true,
                    'constraints' => [
                        new NotBlank(['message' => 'The address is required.']),
                    ],
                ))
                ->add('remarks', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class)
                ->add('status', ChoiceType::class, array(
                    'choices' => array(
                        Status::ShowActive => Status::Active,
                        Status::ShowInActive => Status::InActive
                    ),
                    'required' => true,
        ));
        $builder->add('Create', ButtonType::class);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BooleanLogics\OrganizationBundle\Entity\Employee',
            'cascade_validation' => true
        ));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'organization' => null,
        ));
    }


    /**
     * @return string
     */
    public function getName() {
        return 'booleanlogics_organizationbundle_employee';
    }

}
