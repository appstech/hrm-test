<?php

namespace BooleanLogics\CoreBundle\EventListener;

use BooleanLogics\CoreBundle\Service\SiteManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Description of CurrentSiteListener
 *
 * @author Arslan
 * https://www.youtube.com/watch?v=2BLin8j1kWo
 */
class CurrentSiteListener {

    private $siteManager;
    private $em;
    private $baseHost;

    public function __construct(SiteManager $siteManager, EntityManager $em, $baseHost) {
        $this->siteManager = $siteManager;
        $this->em = $em;
        $this->baseHost = $baseHost;
    }

    public function onKernelRequest(GetResponseEvent $event) {

        $request = $event->getRequest();
        if (!$request->hasSession('_organization')) {
            echo "IN";
            $request->getSession()->set('_organization', $this->getOrganization($event));
            return;
        }
        $request->getSession()->set('_organization', $this->getOrganization($event));
//        if ($locale = $request->attributes->get('_organization')) {
//            $request->getSession()->set('_organization', $locale);
//        } else {
//            $request->setLocale($request->getSession()->get('_organization', $this->getOrganization($event)));
//        }
    }

    private function getOrganization(GetResponseEvent $event) {
        $request = $event->getRequest();
        $host = $request->getHost();
        $baseHost = $this->baseHost;
        $subdomain = str_replace('.' . $baseHost, '', $host);

        $site = $this->em
                ->getRepository('BooleanLogicsOrganizationBundle:Organization')
                ->findOneBy(array('name' => $subdomain));

        if (!$site) {
            throw new NotFoundHttpException(printf(
                    'Cannot find site for host "%s", subdomain "%s"', $host, $subdomain
            ));
        }
        $siteManager = $this->siteManager;
        $siteManager->setCurrentSite($site);
        return $siteManager->getCurrentSite();
    }

}
