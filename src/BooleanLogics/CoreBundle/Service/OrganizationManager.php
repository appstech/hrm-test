<?php

namespace BooleanLogics\CoreBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrganizationManager {

    private $requestStack;
    private $request;
    private $em;

    public function __construct(RequestStack $requestStack, EntityManager $em) {
        $this->requestStack = $requestStack;
        $this->em = $em;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    private function getRequest() {
        return $this->request;
    }

    public function getOrganization() {
        return $this->em
                        ->getRepository('BooleanLogicsOrganizationBundle:Organization')
                        ->find($this->getOrganizationFromSession()->getId());
    }

    private function getOrganizationFromSession() {
        $request = $this->getRequest();
        if (!$request->getSession()->get('_organization')) {
            throw new NotFoundHttpException();
        }
        return $request->getSession()->get('_organization');
    }

    public function getOrganizationId() {
        return $this->getOrganization()->getId();
    }

    public function getOrganizationName() {
        return $this->getOrganization()->getName();
    }

}
