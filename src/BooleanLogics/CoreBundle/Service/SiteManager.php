<?php

namespace BooleanLogics\CoreBundle\Service;

class SiteManager {

    private $currentSite;

    public function setCurrentSite($currentSite) {
        return $this->currentSite = $currentSite;
    }

    public function getCurrentSite() {
        return $this->currentSite;
    }

}
