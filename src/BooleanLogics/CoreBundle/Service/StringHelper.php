<?php

namespace BooleanLogics\CoreBundle\Service;

use Cocur\Slugify\Slugify;

class StringHelper {

    private $string;
    private $slugify;

    public function __construct($string) {
        $this->string = $string;
    }

    public function getSlug() {
        return $this->generateSlug();
    }

    public function getOrganizationID() {
        return $this->generateOrgnizationUniqueID();
    }

    private function generateSlug($spretor = '-') {
        $slugify = new Slugify();
        $this->slugify = $slugify->slugify($this->string, $spretor);
        return $this;
    }

    private function generateOrgnizationUniqueID() {
        $this->generateSlug();
        return $this->slugify . '-' . md5(time() . md5(time()));
    }

}
