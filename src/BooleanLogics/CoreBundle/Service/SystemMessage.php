<?php

namespace BooleanLogics\CoreBundle\Service;

use BooleanLogics\CoreBundle\Model\Message;

class SystemMessage {

    public function getMessage($value, $type) {
        if ($type == 'success') {
            return $this->getSuccessMessage($value);
        }
    }

    private function getSuccessMessage($value) {
        return $this->replaceKey(Message::SUCCESS, "{{name}}", $value);
    }

    private function replaceKey($message, $placeholder, $value) {
        return str_replace($placeholder, $value, $message);
    }

}
