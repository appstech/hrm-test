<?php

namespace BooleanLogics\CoreBundle\Service;

use Symfony\Component\HttpFoundation\Response;

class ResponseHelper {

    private $response;

    public function __construct() {
        $this->response = new Response();
        $this->response->headers->set('Content-Type', 'application/json');
    }

    public function getResponse($array) {
        return $this->response->setContent(json_encode($array));
    }
    
    

}
