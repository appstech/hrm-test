<?php

namespace BooleanLogics\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class AbstractEntity {

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    protected $status;

    /**
     * @var datetime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime")
     */
    protected $modifiedAt;

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Author
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Entity
     * @ORM\PrePersist
     */
    public function setCreatedAt() {
        $dt = new \DateTime();
        $dt->setTimezone(new \DateTimeZone('UTC'));
        $this->createdAt = $dt;


        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Entity
     * @ORM\PrePersist
     * @ORM\PreUpdate 
     */
    public function setModifiedAt() {
        $dt = new \DateTime();
        $dt->setTimezone(new \DateTimeZone('UTC'));
        $this->modifiedAt = $dt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt() {
        return $this->modifiedAt;
    }

    public function getStatusTxt() {
        $statusTxt = '-';
        if ($this->status == 1) {
            $statusTxt = \BooleanLogics\CoreBundle\Model\Status::ShowActive;
        } elseif ($this->status == 0) {
            $statusTxt = \BooleanLogics\CoreBundle\Model\Status::ShowInActive;
        }
        return ucfirst($statusTxt);
    }

}
