<?php

namespace BooleanLogics\CoreBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 * @ORM\Entity
 * @ORM\Table(name="blt_user")
 * @ORM\Entity(repositoryClass="BooleanLogics\CoreBundle\Repository\UserRepository")
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Organization", mappedBy="creator")
     */
    private $organizations;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Branch", mappedBy="creator")
     */
    private $branches;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Department", mappedBy="creator")
     */
    private $departments;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Designation", mappedBy="creator")
     */
    private $designations;

    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\Job", mappedBy="creator")
     */
    private $jobs;
    
    /**
     * @ORM\OneToMany(targetEntity="BooleanLogics\OrganizationBundle\Entity\EmployeeHistory", mappedBy="creator")
     */
    private $employees;

    public function __construct() {
        parent::__construct();
        // your own logic
    }

    /**
     * Add organization
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Organization $organization
     *
     * @return User
     */
    public function addOrganization(\BooleanLogics\OrganizationBundle\Entity\Organization $organization) {
        $this->organizations[] = $organization;

        return $this;
    }

    /**
     * Remove organization
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Organization $organization
     */
    public function removeOrganization(\BooleanLogics\OrganizationBundle\Entity\Organization $organization) {
        $this->organizations->removeElement($organization);
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizations() {
        return $this->organizations;
    }

    /**
     * Add branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     *
     * @return User
     */
    public function addBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch) {
        $this->branches[] = $branch;

        return $this;
    }

    /**
     * Remove branch
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Branch $branch
     */
    public function removeBranch(\BooleanLogics\OrganizationBundle\Entity\Branch $branch) {
        $this->branches->removeElement($branch);
    }

    /**
     * Get branches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBranches() {
        return $this->branches;
    }

    /**
     * Add department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     *
     * @return User
     */
    public function addDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department) {
        $this->departments[] = $department;

        return $this;
    }

    /**
     * Remove department
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Department $department
     */
    public function removeDepartment(\BooleanLogics\OrganizationBundle\Entity\Department $department) {
        $this->departments->removeElement($department);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartments() {
        return $this->departments;
    }

    /**
     * Add designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     *
     * @return User
     */
    public function addDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations[] = $designation;

        return $this;
    }

    /**
     * Remove designation
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Designation $designation
     */
    public function removeDesignation(\BooleanLogics\OrganizationBundle\Entity\Designation $designation) {
        $this->designations->removeElement($designation);
    }

    /**
     * Get designations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignations() {
        return $this->designations;
    }


    /**
     * Add job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     *
     * @return User
     */
    public function addJob(\BooleanLogics\OrganizationBundle\Entity\Job $job)
    {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove job
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\Job $job
     */
    public function removeJob(\BooleanLogics\OrganizationBundle\Entity\Job $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     *
     * @return User
     */
    public function addEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee
     */
    public function removeEmployee(\BooleanLogics\OrganizationBundle\Entity\EmployeeHistory $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }
}
