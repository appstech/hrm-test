<?php

namespace BooleanLogics\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * @Route("/", name="core_home")
     */
    public function indexAction(Request $request) {

        //$this->container->get('request')->getSession()->get('_organization');

        return $this->container->get('organization_manager')->getOrganizationName();

        return $orgnization = $request->getSession()->get('_organization');
        return $this->render('BooleanLogicsCoreBundle:Default:index.html.twig');
    }

}
