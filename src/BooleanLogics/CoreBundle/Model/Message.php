<?php

namespace BooleanLogics\CoreBundle\Model;

/**
 * Description of Status
 *
 * @author arslan.arif
 */
abstract class Message {

    const SUCCESS = "{{name}} created successfully.";

}
