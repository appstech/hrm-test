<?php

namespace BooleanLogics\CoreBundle\Model;

/**
 * Description of Status
 *
 * @author arslan.arif
 */
abstract class Status {

    const Active = 1;
    const InActive = 0;
    const ShowActive = 'Active';
    const ShowInActive = 'Inactive';

}
