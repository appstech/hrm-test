<?php

namespace BooleanLogics\CoreBundle\Model;

/**
 * Description of ListManager
 *
 * @author arslan.arif
 */
class DatatableListManager {

    public function sanitizeMemberListDatatable($draw, array $columns, array $order = null, $start = null, $length = null, array $search = null) {
        $searchString = null;

        $dir = array(
            'desc' => 'DESC',
            'asc' => 'ASC'
        );

        $orderBy = array();

        if (null !== $search['value'] && "" !== trim($search['value'])) {
            $searchString = $search['value'];
        }

        foreach ($order as $oneOrder) {
            if ('id' === $columns[$oneOrder['column']]['data'] ||
                    'email' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => $columns[$oneOrder['column']]['data'],
                    'dir' => $dir[$oneOrder['dir']]
                );
            } elseif ('first_name' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => 'firstName',
                    'dir' => $dir[$oneOrder['dir']]
                );
            } elseif ('last_name' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => 'lastName',
                    'dir' => $dir[$oneOrder['dir']]
                );
            }
        }

        return array(
            'draw' => (int) $draw,
            'start' => (int) $start,
            'length' => (int) $length,
            'order' => $orderBy,
            'search' => $searchString
        );
    }

    public function sanitizeTagListDatatable($draw, array $columns, array $order = null, $start = null, $length = null, array $search = null) {
        $searchString = null;

        $dir = array(
            'desc' => 'DESC',
            'asc' => 'ASC'
        );

        $orderBy = array();

        if (null !== $search['value'] && "" !== trim($search['value'])) {
            $searchString = $search['value'];
        }

        foreach ($order as $oneOrder) {
            if ('id' === $columns[$oneOrder['column']]['data'] ||
                    'name' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => $columns[$oneOrder['column']]['data'],
                    'dir' => $dir[$oneOrder['dir']]
                );
            }
        }

        return array(
            'draw' => (int) $draw,
            'start' => (int) $start,
            'length' => (int) $length,
            'order' => $orderBy,
            'search' => $searchString
        );
    }

    public function sanitizePageListDatatable($draw, array $columns, array $order = null, $start = null, $length = null, array $search = null) {
        $searchString = null;

        $dir = array(
            'desc' => 'DESC',
            'asc' => 'ASC'
        );

        $orderBy = array();

        if (null !== $search['value'] && "" !== trim($search['value'])) {
            $searchString = $search['value'];
        }

        foreach ($order as $oneOrder) {
            if ('id' === $columns[$oneOrder['column']]['data'] ||
                    'title' === $columns[$oneOrder['column']]['data'] ||
                    'titleTag' === $columns[$oneOrder['column']]['data'] ||
                    'sefURL' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => $columns[$oneOrder['column']]['data'],
                    'dir' => $dir[$oneOrder['dir']]
                );
            }
        }

        return array(
            'draw' => (int) $draw,
            'start' => (int) $start,
            'length' => (int) $length,
            'order' => $orderBy,
            'search' => $searchString
        );
    }

    public function sanitizeEmailListDatatable($draw, array $columns, array $order = null, $start = null, $length = null, array $search = null) {
        $searchString = null;

        $dir = array(
            'desc' => 'DESC',
            'asc' => 'ASC'
        );

        $orderBy = array();

        if (null !== $search['value'] && "" !== trim($search['value'])) {
            $searchString = $search['value'];
        }

        foreach ($order as $oneOrder) {
            if ('id' === $columns[$oneOrder['column']]['data'] ||
                    'title' === $columns[$oneOrder['column']]['data'] ||
                    'subject' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => $columns[$oneOrder['column']]['data'],
                    'dir' => $dir[$oneOrder['dir']]
                );
            }
        }

        return array(
            'draw' => (int) $draw,
            'start' => (int) $start,
            'length' => (int) $length,
            'order' => $orderBy,
            'search' => $searchString
        );
    }

    public function sanitizeJourneyTitlesListDatatable($draw, array $columns, array $order = null, $start = null, $length = null, array $search = null) {
        $searchString = null;

        $dir = array(
            'desc' => 'DESC',
            'asc' => 'ASC'
        );

        $orderBy = array();

        if (null !== $search['value'] && "" !== trim($search['value'])) {
            $searchString = $search['value'];
        }

        foreach ($order as $oneOrder) {
            if ('id' === $columns[$oneOrder['column']]['data'] ||
                    'name' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => $columns[$oneOrder['column']]['data'],
                    'dir' => $dir[$oneOrder['dir']]
                );
            }
        }

        return array(
            'draw' => (int) $draw,
            'start' => (int) $start,
            'length' => (int) $length,
            'order' => $orderBy,
            'search' => $searchString
        );
    }

    public function sanitizeJourneyTemplateListDatatable($draw, array $columns, array $order = null, $start = null, $length = null, array $search = null) {
        $searchString = null;

        $dir = array(
            'desc' => 'DESC',
            'asc' => 'ASC'
        );

        $orderBy = array();

        if (null !== $search['value'] && "" !== trim($search['value'])) {
            $searchString = $search['value'];
        }

        foreach ($order as $oneOrder) {
            if ('id' === $columns[$oneOrder['column']]['data'] ||
                    'name' === $columns[$oneOrder['column']]['data']) {
                $orderBy[] = array(
                    'field' => $columns[$oneOrder['column']]['data'],
                    'dir' => $dir[$oneOrder['dir']]
                );
            }
        }

        return array(
            'draw' => (int) $draw,
            'start' => (int) $start,
            'length' => (int) $length,
            'order' => $orderBy,
            'search' => $searchString
        );
    }

}
